public class TestGeometricObject {
	/** Main method */
	public static void main(String[] args) {
		// Create two Circle objects
		Circle circle1 = new Circle(3, "red", true);
		Circle circle2 = new Circle(5, "blue", false);

		// Display larger circle
		print("\nThe larger of the two circles is ");
		print(Circle.max(circle1, circle2));

		// Create two Rectangle objects
		Rectangle rectangle1 = new Rectangle(20, 10, "green", true);
		Rectangle rectangle2 = new Rectangle(4.2, 5, "orange", true);

		// Display larger rectangle
		print("\nThe larger of the two rectangles is ");
		print(Rectangle.max(rectangle1, rectangle2));


		// Create two Triangle objects
		Triangle triangle1 = new Triangle(5, 8, 4, "yellow", true);
		Triangle triangle2 = new Triangle(6,5,7, "blue", true);


		// Display larger triangle1
		print("\nThe larger of the two triangles is ");
		print(Triangle.max(triangle1, triangle2));

		// Display larger object
		print("\nThe largest  geometric objects is: ");

		//test
		if ((rectangle1.getArea() > circle1.getArea()) && (rectangle1.getArea() > circle2.getArea())
				&& (rectangle1.getArea() > rectangle2.getArea())
				&& (rectangle1.getArea() > triangle1.getArea())
				&& rectangle1.getArea() > triangle2.getArea())
		{
			System.out.println("Rectangle 1.");
		}
		else if((rectangle2.getArea() > circle1.getArea()) && (rectangle2.getArea() > circle2.getArea())
				&& (rectangle2.getArea() > rectangle1.getArea())
				&& (rectangle2.getArea() > triangle1.getArea())
				&& rectangle2.getArea() > triangle2.getArea())
		{
			System.out.println("Rectangle 2.");
		}
		else if ((circle1.getArea() > circle2.getArea()) && (circle1.getArea() > rectangle1.getArea())
				&& (circle1.getArea() > rectangle2.getArea())
				&& (circle1.getArea() > triangle1.getArea())
				&& circle1.getArea() > triangle2.getArea())
		{
			System.out.println("Circle 1.");
		}
		else if ((circle2.getArea() > circle1.getArea()) && (circle2.getArea() > rectangle1.getArea())
				&& (circle2.getArea() > rectangle2.getArea())
				&& (circle2.getArea() > triangle1.getArea())
				&& circle2.getArea() > triangle2.getArea())
		{
			System.out.println("Circle 2.");
		}
		else if ((triangle1.getArea() > triangle2.getArea()) && (triangle1.getArea() > rectangle1.getArea())
				&& (triangle1.getArea() > rectangle2.getArea())
				&& (triangle1.getArea() > circle1.getArea())
				&& triangle1.getArea() > circle2.getArea())
		{
			System.out.println("Triangle 1.");
		}
		else if ((triangle2.getArea() > triangle1.getArea()) && (triangle2.getArea() > rectangle1.getArea())
				&& (triangle2.getArea() > rectangle2.getArea())
				&& (triangle2.getArea() > circle1.getArea())
				&& triangle2.getArea() > circle2.getArea())
		{
			System.out.println("Triangle 2.");
		}


	}

	// Displays a string
	public static void print(String s) {
		System.out.println(s);
	}

	// Displays a GeometricObject
	public static void print(GeometricObject o) {
		System.out.println(o);
	}
}


